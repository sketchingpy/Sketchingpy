Third Party (Site)
====================================================================================================
Third party sources included for the website. Licenses:

 - [Ace Editor](https://ace.c9.io/) under the [BSD License](https://github.com/ajaxorg/ace/blob/master/LICENSE).
 - [IBM Plex](https://www.ibm.com/plex/) under the [OFL License](https://github.com/IBM/plex/blob/master/LICENSE.txt).

These are snapshotted for convience as they can be difficult to assemble into a single place but this directory may be removed in the future.
