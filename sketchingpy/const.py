"""Global constants for sketchingpy.

License:
    BSD
"""

RADIANS = 1
DEGREES = 2

ANGLE_MODES = {
    'radians': RADIANS,
    'degrees': DEGREES
}

CENTER = 11
RADIUS = 12
CORNER = 13
CORNERS = 14

SHAPE_MODES = {
    'center': CENTER,
    'radius': RADIUS,
    'corner': CORNER,
    'corners': CORNERS
}

LEFT = 21
RIGHT = 22
TOP = 23
BASELINE = 24
BOTTOM = 25

ALIGN_OPTIONS = {
    'left': LEFT,
    'center': CENTER,
    'right': RIGHT,
    'top': TOP,
    'baseline': BASELINE,
    'bottom': BOTTOM
}

MOUSE_LEFT_BUTTON = 'leftMouse'
MOUSE_RIGHT_BUTTON = 'rightMouse'

KEYBOARD_LEFT_BUTTON = 'left'
KEYBOARD_UP_BUTTON = 'up'
KEYBOARD_RIGHT_BUTTON = 'right'
KEYBOARD_DOWN_BUTTON = 'down'
KEYBOARD_SPACE_BUTTON = 'space'
KEYBOARD_CTRL_BUTTON = 'ctrl'
KEYBOARD_ALT_BUTTON = 'alt'
KEYBOARD_SUPER_BUTTON = 'super'
KEYBOARD_SHIFT_BUTTON = 'shift'
KEYBOARD_TAB_BUTTON = 'tab'
KEYBOARD_HOME_BUTTON = 'home'
KEYBOARD_END_BUTTON = 'end'
KEYBOARD_RETURN_BUTTON = 'return'
KEYBOARD_BACKSPACE_BUTTON = 'backspace'
