import sketchingpy


sketch = sketchingpy.Sketch2DStatic(500, 500)

sketch.clear('#505050')

sketch.set_fill('#C0C0C070')
sketch.set_stroke('#00000050')
sketch.set_stroke_weight(2)

sketch.set_rect_mode('radius')
sketch.set_fill('#A6CEE370')
sketch.draw_rect(250, 250, 30, 20)

sketch.set_rect_mode('center')
sketch.set_fill('#1F78B470')
sketch.draw_rect(250, 250, 20, 60)

sketch.set_rect_mode('corner')
sketch.set_fill('#B2DF8A70')
sketch.draw_rect(250, 250, 60, 60)

sketch.set_rect_mode('corners')
sketch.set_fill('#33A02C70')
sketch.draw_rect(250, 250, 20, 30)

sketch.save_image('shapes_rect.png')