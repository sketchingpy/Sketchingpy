Static Examples
================================================================================
Static rendering examples for the `Sketchingpy` library which write to images without requiring the need to show a canvas or window.
