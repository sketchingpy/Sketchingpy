import sketchingpy
import sketchingpy.geo


sketch = sketchingpy.Sketch2DStatic(500, 500)
sketch.clear('#F0F0F0')

# Center the map on San Francisco and place in middle of sketch
center_longitude = -122.418343
center_latitude = 37.761842
center_x = 250
center_y = 250
map_scale = 10

# Create a geo transformation that ties pixel to geo coordinates
sketch.set_map_pan(center_longitude, center_latitude) # Where to center geographically
sketch.set_map_zoom(map_scale)  # How much zoom
sketch.set_map_placement(center_x, center_y)  # Where to center in terms of pixels

# Make and convert points
geo_polygon = sketch.start_geo_polygon(-122, 38)
geo_polygon.add_coordinate(-121, 38)
geo_polygon.add_coordinate(-121, 37)
geo_polygon.add_coordinate(-122, 37)
shape = geo_polygon.to_shape()

# Draw
sketch.set_fill('#333333')
sketch.clear_stroke()
sketch.draw_shape(shape)

sketch.save_image('geo_polygon.png')
