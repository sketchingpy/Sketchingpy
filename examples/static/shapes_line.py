import sketchingpy


sketch = sketchingpy.Sketch2DStatic(500, 500)

sketch.clear('#505050')

sketch.set_stroke('#A6CEE370')
sketch.set_stroke_weight(2)
sketch.draw_line(150 - 50, 150, 350 + 50, 150)

sketch.set_stroke('#1F78B470')
sketch.set_stroke_weight(4)
sketch.draw_line(350, 150 - 50, 350, 350 + 50)

sketch.set_stroke('#B2DF8A70')
sketch.set_stroke_weight(8)
sketch.draw_line(350 + 50, 350, 150 - 50, 350)

sketch.set_stroke('#33A02C70')
sketch.set_stroke_weight(10)
sketch.draw_line(150, 350 + 50, 150, 150 - 50)

sketch.save_image('shapes_line.png')