import sketchingpy


sketch = sketchingpy.Sketch2DStatic(500, 500)

image = sketch.get_image('./reference.png')
image.resize(150, 150)

sketch.clear('#707070')

sketch.clear_stroke()
sketch.set_ellipse_mode('radius')
sketch.set_fill('#A6CEE350')
sketch.draw_ellipse(250, 250, 100, 100)

sketch.set_image_mode('center')
sketch.draw_image(250, 250, image)

sketch.save_image('image_draw.png')