import sketchingpy


sketch = sketchingpy.Sketch2DStatic(500, 500)

sketch.clear('#505050')

sketch.set_stroke('#F0F0F050')
sketch.draw_line(50, 250, 450, 250)

sketch.set_text_font('./IBMPlexMono-Regular.ttf', 20)
sketch.clear_stroke()

sketch.set_fill('#33A02CA0')
sketch.set_text_align('center', 'top')
sketch.draw_text(100, 250, 'Sketchingpy')

sketch.set_fill('#A6CEE3A0')
sketch.set_text_align('center', 'center')
sketch.draw_text(200, 250, 'Sketchingpy')

sketch.set_fill('#B2DF8AA0')
sketch.set_text_align('center', 'baseline')
sketch.draw_text(300, 250, 'Sketchingpy')

sketch.set_fill('#1f78b4')
sketch.set_text_align('center', 'bottom')
sketch.draw_text(400, 250, 'Sketchingpy')

sketch.save_image('text_vert_align.png')