import time

import sketchingpy

HALF_SEGMENT_HEIGHT = 62
SEGMENT_HEIGHT = HALF_SEGMENT_HEIGHT * 2


sketch = sketchingpy.Sketch2D(500, 500)
sketch.set_rect_mode('corner')


def draw_component(bg_color, start_y, multiplier):
    sketch.clear_stroke()
    sketch.set_fill(bg_color)
    sketch.draw_rect(10, start_y, 480, SEGMENT_HEIGHT - 3)
    
    sketch.clear_fill()
    sketch.set_stroke('#505050')
    sketch.set_stroke_weight(3)
    millis = sketch.get_millis_shown()  # Always zero in static context

    end_y = start_y + SEGMENT_HEIGHT - 3
    for x in range(10, 480, 25 * multiplier):
        x_offset = millis / 50 / multiplier
        x_offset_wrapped = x_offset % (25 * multiplier)
        actual_x = x + x_offset_wrapped
        sketch.draw_line(actual_x, start_y, actual_x, end_y)


def draw_all():
    sketch.clear('#505050')

    draw_component('#A6CEE350', SEGMENT_HEIGHT * 0 + HALF_SEGMENT_HEIGHT, 1)
    draw_component('#1F78B450', SEGMENT_HEIGHT * 1 + HALF_SEGMENT_HEIGHT, 2)
    draw_component('#B2DF8A50', SEGMENT_HEIGHT * 2 + HALF_SEGMENT_HEIGHT, 3)


draw_all()

sketch.save_image('timing.png')
