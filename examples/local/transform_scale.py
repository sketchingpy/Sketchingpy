import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')

sketch.set_stroke('#F0F0F0')
sketch.set_rect_mode('radius')

sketch.set_fill('#A6CEE350')
sketch.draw_rect(100, 100, 50, 50)

sketch.set_fill('#1F78B450')
sketch.scale(1.5)
sketch.draw_rect(100, 100, 50, 50)

sketch.set_fill('#B2DF8A50')
sketch.scale(2)
sketch.draw_rect(100, 100, 50, 50)

sketch.show()