import sketchingpy

GROUPS = ['a', 'b', 'c', 'd']
GROUP_POSITIONS = {
    'axis': 50,
    'a': 100,
    'b': 200,
    'c': 300,
    'd': 400
}
MAX_VALUE = 4
MIN_VALUE = 0
START_Y = 50
END_Y = 450


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#F0F0F0')

sketch.set_text_font('./IBMPlexMono-Regular.ttf', 12)

# Create scales
def get_x(value):
    return GROUP_POSITIONS[value]

def get_y(value):
    percent_through_scale = (value - MIN_VALUE) / (MAX_VALUE - MIN_VALUE)
    return (START_Y - END_Y) * percent_through_scale + END_Y

# Axis setup
sketch.clear_stroke()
sketch.set_fill('#333333')

# Draw left axis
sketch.set_text_align('right', 'center')
for i in range(MIN_VALUE, MAX_VALUE + 1):
    sketch.draw_text(get_x('axis'), get_y(i), '%.1f' % i)

# Draw bottom axis
sketch.set_text_align('center', 'top')
for group in GROUPS:
    sketch.draw_text(get_x(group), get_y(MIN_VALUE) + 20, group.upper())

# Bar setup
sketch.set_fill('#A0A0A0')
sketch.set_rect_mode('corners')

# Draw bars
records = sketch.get_data_layer().get_csv('example.csv')
for record in records:
    group = record['group']
    value_str = record['value']
    value = float(value_str)

    x = get_x(group)
    y = get_y(value)
    sketch.draw_rect(x - 10, y, x + 10, END_Y)

sketch.show()