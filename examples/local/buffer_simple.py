import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')

sketch.create_buffer('circles', 100, 100)
sketch.enter_buffer('circles')
sketch.set_stroke_weight(3)
sketch.set_fill('#B2DF8A50')
sketch.set_stroke('#F0F0F0')
sketch.set_ellipse_mode('radius')
sketch.draw_ellipse(30, 50, 25, 25)
sketch.draw_ellipse(70, 50, 25, 25)

sketch.exit_buffer()
sketch.draw_buffer(100, 100, 'circles')
sketch.draw_buffer(300, 100, 'circles')
sketch.draw_buffer(100, 300, 'circles')
sketch.draw_buffer(300, 300, 'circles')

sketch.show()