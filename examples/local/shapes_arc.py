import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')
sketch.set_angle_mode('degrees')

sketch.clear_fill()
sketch.set_stroke_weight(5)

sketch.set_arc_mode('radius')
sketch.set_stroke('#A6CEE370')
sketch.draw_arc(250, 250, 200, 200, 0, 120)

sketch.set_arc_mode('center')
sketch.set_stroke('#1F78B470')
sketch.draw_arc(250, 250, 200, 200, 90, 210)

sketch.set_arc_mode('corner')
sketch.set_stroke('#B2DF8A70')
sketch.draw_arc(250, 250, 200, 200, 180, 300)

sketch.set_arc_mode('corners')
sketch.set_stroke_weight(1)
sketch.set_stroke('#333333')
sketch.set_fill('#33A02C70')
sketch.draw_arc(250, 250, 0, 0, 240, 360)

sketch.show()