import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')

sketch.set_stroke('#F0F0F0')
sketch.set_ellipse_mode('radius')
sketch.set_angle_mode('degrees')

sketch.set_fill('#A6CEE350')
sketch.draw_ellipse(0, 0, 100, 100)

sketch.set_fill('#1F78B450')
sketch.translate(250, 250)
sketch.draw_ellipse(0, 0, 100, 100)

sketch.show()