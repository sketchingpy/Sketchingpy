import sketchingpy


records = [
    {'group': 'a', 'value': 1},
    {'group': 'b', 'value': 2},
    {'group': 'c', 'value': 3},
    {'group': 'd', 'value': 4}
]

sketch = sketchingpy.Sketch2DApp(500, 500)
sketch.get_data_layer().write_csv(records, ['group', 'value'], 'test.csv')
