import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')

sketch.set_stroke('#F0F0F050')
sketch.draw_line(250, 50, 250, 450)

sketch.set_text_font('./IBMPlexMono-Regular.ttf', 20)
sketch.clear_stroke()

sketch.set_fill('#33A02CA0')
sketch.set_text_align('left')
sketch.draw_text(250, 150, 'Sketchingpy')

sketch.set_fill('#A6CEE3A0')
sketch.set_text_align('center')
sketch.draw_text(250, 250, 'Sketchingpy')

sketch.set_fill('#B2DF8AA0')
sketch.set_text_align('right')
sketch.draw_text(250, 350, 'Sketchingpy')

sketch.show()