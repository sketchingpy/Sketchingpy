import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')

sketch.set_fill('#C0C0C070')
sketch.set_stroke('#00000070')

sketch.set_ellipse_mode('radius')
sketch.set_fill('#A6CEE370')
sketch.draw_ellipse(250, 250, 20, 20)

sketch.set_ellipse_mode('center')
sketch.set_fill('#1F78B470')
sketch.draw_ellipse(250, 250, 20, 20)

sketch.set_ellipse_mode('corner')
sketch.set_fill('#B2DF8A70')
sketch.draw_ellipse(250, 250, 20, 20)

sketch.set_ellipse_mode('corners')
sketch.set_fill('#33A02C70')
sketch.draw_ellipse(250, 250, 20, 20)

sketch.show()