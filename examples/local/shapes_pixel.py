import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')
sketch.set_fill('#FFFFFF')

for x in range(100, 400, 5):
    sketch.draw_pixel(x, 250)

sketch.show()