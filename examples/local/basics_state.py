import sketchingpy


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.clear('#505050')
sketch.set_stroke_weight(3)

sketch.set_fill('#B2DF8A50')
sketch.set_stroke('#333333')
sketch.draw_ellipse(150, 250, 20, 20)

sketch.push_style()
sketch.clear_fill()
sketch.draw_ellipse(250, 250, 20, 20)
sketch.pop_style()

sketch.draw_ellipse(350, 250, 20, 20)

sketch.show()