import sketchingpy
import time

# Make the sketch
sketch = sketchingpy.Sketch2D(500, 500)
sketch.clear('#F0F0F0')
sketch.clear_fill()
sketch.set_angle_mode('degrees')
start_time = time.time()

def draw(sketch):
  """Draw sketch"""
  mouse = sketch.get_mouse()
  x = mouse.get_pointer_x()
  y = mouse.get_pointer_y()
  
  # Draw from center to the user's mouse (or location of tap on mobile)
  sketch.set_stroke_weight(1)
  sketch.set_stroke('#33333350')
  sketch.draw_line(x, y, 250, 250)
  
  # Draw arc
  sketch.set_stroke_weight(10)
  sketch.set_stroke('#A0A0C010')
  start_angle = (time.time() - start_time) * 30
  sketch.draw_arc(250, 250, 200, 200, start_angle, start_angle + 2)

# Register the draw method and show
sketch.on_step(draw)
sketch.show()