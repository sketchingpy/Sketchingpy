import sketchingpy
import sketchingpy.const


sketch = sketchingpy.Sketch2DApp(500, 500)

sketch.set_text_font('./IBMPlexMono-Regular.ttf', 20)
sketch.set_text_align('center', 'center')
sketch.clear_stroke()

keyboard = sketch.get_keyboard()
press_states = {
    sketchingpy.const.KEYBOARD_LEFT_BUTTON: False,
    'w': False
}

def handle_press(button):
    press_states[button.get_name()] = True

keyboard.on_key_press(handle_press)

def draw(sketch):
    pressed_list = list(keyboard.get_keys_pressed())
    no_keys_pressed = len(pressed_list) == 0
    if no_keys_pressed:
        currently_pressed_str = 'None'
    else:
        pressed_list_str = [x.get_name() for x in pressed_list]
        currently_pressed_str = ', '.join(pressed_list_str)

    if press_states[sketchingpy.const.KEYBOARD_LEFT_BUTTON]:
        left_str = 'Press of left key seen.'
    else:
        left_str = 'Waiting for left press.'

    if press_states['w']:
        w_str = 'Press of w key seen.'
    else:
        w_str = 'Waiting for w key.'

    sketch.clear('#505050')

    sketch.set_fill('#B2DF8A')
    sketch.set_text_font('./IBMPlexMono-Regular.ttf', 12)
    sketch.draw_text(250, 100, 'Click into sketch canvas and use keys.')

    sketch.set_text_font('./IBMPlexMono-Regular.ttf', 20)

    sketch.set_fill('#A6CEE3')
    sketch.draw_text(250, 200, left_str)

    sketch.set_fill('#A6CEE3')
    sketch.draw_text(250, 300, w_str)

    sketch.set_fill('#B2DF8A')
    sketch.draw_text(250, 400, 'Currently pressed: ' + currently_pressed_str)

sketch.on_step(draw)
sketch.show()