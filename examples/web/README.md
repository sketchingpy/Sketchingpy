Web Examples
================================================================================
Browser-based examples for the `Sketchingpy` library. Uses [pyscript](https://pyscript.net/) under the [Apache v2 License](https://pyscript.github.io/docs/2023.11.2/license/) and [Ace Editor](https://ace.c9.io/) under the [BSD License](https://github.com/ajaxorg/ace/blob/master/LICENSE) for the web editor.
