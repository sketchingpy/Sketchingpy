let firstRun = true;

const REPO_BASE_URL = "https://codeberg.org/sketchingpy/Sketchingpy/src/branch/main/examples";
const LOCAL_BASE_URL = REPO_BASE_URL + "/local/";
const STATIC_BASE_URL = REPO_BASE_URL + "/static/";


function main() {
    const editor = initEditor();
    addEventListeners(editor);

    const urlParams = new URLSearchParams(window.location.search);
    const sketchName = urlParams.get('sketch');
    if (sketchName !== null) {
        document.getElementById("sketch-select").value = sketchName;
    }

    loadProgramCode(editor, true);

    window.addEventListener("error", (error) => {
        const display = document.getElementById("runtime-error-display");
        const escaped = new Option(error.message).innerHTML;
        display.innerHTML = escaped;
        display.style.display = "block";
    });
}


function initEditor() {
    const editor = ace.edit("editor");
    editor.getSession().setMode("ace/mode/python");
    editor.setOption("enableKeyboardAccessibility", true);
    editor.commands.removeCommand("find");
    return editor;
}


function runProgram(editor) {
    showLoading();
    showFirstLoad();

    const rawCode = editor.getValue();

    const wrappedCode = "<script type='py' id='code-tag'>\n" + rawCode + "\n</script>";
    document.getElementById("main-script").innerHTML = wrappedCode;

    document.querySelectorAll(".py-error").forEach((x) => x.remove());
    document.getElementById("runtime-error-display").style.display = "none";

    setTimeout(() => {
        showLoaded(false, () => document.getElementById("sketch-canvas").scrollIntoView());
    }, 500);
}


function addEventListeners(editor) {
    document.getElementById("sketch-select").addEventListener(
        "change",
        () => loadProgramCode(editor, false)
    );
    
    document.getElementById("run-button").addEventListener("click", () => runProgram(editor));
}


function loadProgramCode(editor, runOnLoad) {
    showLoading();

    const selected = document.getElementById("sketch-select").value;
    const version = document.getElementById("version").value;
    const path = "/examples/web/py/" + selected + ".pyscript?v=" + version;

    const repoFilename = selected + ".py";
    
    const localExampleUrl = LOCAL_BASE_URL + repoFilename;
    document.getElementById("local-link").href = localExampleUrl;

    const staticExampleUrl = STATIC_BASE_URL + repoFilename;
    document.getElementById("static-link").href = staticExampleUrl;
    
    fetch(path)
        .then((response) => {
            if (!response.ok) {
                throw "Failed to load program."
            }
            return response.text();
        })
        .then((code) => {
            editor.setValue(code, -1);
            showLoaded();
            history.pushState(
                null,
                "",
                "/examples/web/example.html?sketch=" + selected
            );
        })
        .catch((x) => {
            alert('Failed to load program.');
        });
}


function showLoading() {
    document.getElementById("loading-msg").style.display = "inline-block";
    document.getElementById("loaded-msg").style.display = "none";
}


function showLoaded() {
    document.getElementById("loading-msg").style.display = "none";
    document.getElementById("loaded-msg").style.display = "inline-block";
}


function showFirstLoad() {
    document.getElementById("sketch-load-message").style.display = "block";

    const loadingContent = document.getElementById("sketch-load-message-content");
    loadingContent.innerHTML = "Loading environment...";

    const progressBar = document.getElementById("sketch-load-progress");
    progressBar.style.display = "inline-block";
    progressBar.value = 0;
    const incrementBar = () => {
        progressBar.value += 1;

        if (progressBar.value < 19) {
            setTimeout(incrementBar, 500);
        }
    };
    incrementBar();
}


main();