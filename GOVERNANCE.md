# Governance
Though the project seeks to operate informally and by conensus, we understand that disagreements may emerge. For some actions, this project operates with through a "planning" group primarily made up of maintainers.

<br>

## Structure
Everyone is welcome to participate in the community including governance. That said, there are some defined positions in the governance structure:

 - **Community member**: Anyone with an interest in Sketchingpy is a community member. We encourage everyone to engage with thoughts and feedback through the Sketchingpy community.
 - **Contributor**: Anyone who has given code, documentation, or examples to the repository but not yet part of the planning group.
 - **Community representative**: Individuals invited by the planning group for a 6 month limited term renewable which can be renewed only after the term has expired. Each community representative gets a single vote.
 - **Regular maintainer**: A frequent contributor who has been added to the planning group and has write access against the repository. Each regular maintainer gets a single vote.
 - **Lead maintainer**: A maintainer who can break ties. This position is maintained as long as the person is a maintainer after which the group elects a new lead.

These roles operate as defined below.

<br>

## Promotion
The following process is available for promotion of users which is to say moving a community member from one of the earlier bullet points above in structure to one of the later bullet points:

 - Any contributor can be made into a maintainer by adoption of a resolution by the current planning group.
 - Any non-maintainer community member can be made into a community representative by adoption of a resolution by the current planning group.
 - A regular maintainer can be made into a lead maintainer if there is not currently another lead maintainer.

Other changes in user status require the removal process.

<br>

## Removal
The planning group may take the following actions by adopting a resolution:

 - A community member may be removed from community (banned) if they are not on planning group.
 - A planning group member may also be converted back to a contributor.

Removal is meant as a last resort and efforts should be taken to resolve conflict through other means first when possible.

<br>

## Regular operation
Maintainers help ensure the conceptual integrity and stability of the project. They can approve pull requests from themselves, community members, and contributors. The process for adopting changes to the code base including documentation:

 - Optionally, post an issue describing the task.
 - Optionally, maintainers can either approve the issue, set it to request additional feedback or changes, or deny the task by closing the issue with comment regarding rationale.
 - Post a pull request.
 - Maintainers can either approve a related pull request, set it to request additional feedback or changes, or deny the task by closing the PR with comment regarding rationale.
 - Maintainers can remove approval prior to merge.

A pull request may be merged if all of the following are met:

 - The pull request is approved by a maintainer for at least 24 hours or the lead maintainer approves merge without waiting period.
 - Disagreements have been resolved between maintainers regarding the merge or the planning group adopts a resolution to merge.

If maintainers disagree on an issue or PR, it will go to a resolution of the current planning group.

<br>

## Releases
A new release to Pypi may be made by adoption of a resolution by the planning group or by decision by the lead maintainer.

<br>

## Voting
Decisions will be made by resolution of the planning group and require majority vote (more than 50% of votes or 50% of votes plus the lead maintainer tie-breaker) for adoption. Any planning group member may submit a resolution. Votes will be taken asynchronously by email or Discord with all group members as recipients. Voting is over when any of the following are met:

 - 168 hours have ellapsed since voting began and more than 50% of planning group members have voted.
 - All planning group members have voted.

That said, voting may end early if all of the following are met:

 - Votes are unanimously in favor or against excluding any members voting as abstaining.
 - Votes from more than 50% of planning group members have been cast.
 - Voting has been open for at least 48 hours.
 - The resolution does not remove a maintainer from the planning group.
 - The resolution does not remove the lead maintainer from that position.

Resolutions are immutable. The member submitting the resolution may also retract it prior to adoption, casuing voting to end with the resolution rejected. All non-retracted resolutions must be posted publicly.

<br>

## Current members
The current members of the planning group: Sam Pottinger.
