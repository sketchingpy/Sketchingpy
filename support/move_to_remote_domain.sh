sed -i 's|http://localhost:8000/|https://editor.sketchingpy.org/|g' editor_src/service_worker.js
sed -i 's|http://localhost:8000/|https://editor.sketchingpy.org/|g' editor_src/sketch.html
sed -i 's|http://localhost:8000/|https://sketchingpy.org/|g' examples/web/example.html
sed -i 's|http://localhost:8000/|https://sketchingpy.org/|g' website_src/base.html